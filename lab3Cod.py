#from numpy.core.numeric import roll
import serial
import numpy
import matplotlib.pyplot as plt
from copy import copy, deepcopy
import time
import math

#API KEY
myAPI='B3Q33ZDZ8CGTW5SR'
baseURL='https://thingspeak.com/channels/1397173/api_keys'%myAPI


A=0.6
B=0.4
dt=0.01

data = serial.Serial(port='COM3', baudrate=9600, timeout=10)#conexion desde vs code
#data = serial.Serial('/dev/ttyACM0',9600,timeout=10)#conexion desde la rasberry
print(data)

datos = numpy.zeros((201,8))
datos1 = numpy.zeros((201,8))
datos2 = numpy.zeros((201,8))
roll=numpy.zeros((202,4))
pitch=numpy.zeros((202,4))

value = input("\nQuiere adquirir los offsets s/n \n\n")
if value == 's':
    for j in range(2):
        print("\nCapturando datos \n")
        data.write(b'c')
        offseta = data.readline()
        sobra = data.readline()
        offseta = offseta.decode("utf-8")
        offseta= offseta.split(";")
    print(offseta,"\n")
    print(sobra,"\n")
    offset=[]
    for i in offseta:
        a=float(i)
        offset.append(a)
    print(offset,"\n")
value1 = input("\nQuiere adquirir los datos s/n \n\n")
if value1 == 's':
    data.write(b"h")    
    for i in range(201):
        x=data.readline()
        x=x.decode("utf-8")
        x=x.split(";")
        datos1[i][:]=x
    print(datos1,"\n")
    datos=deepcopy(datos1)
c = numpy.savetxt('geekfile.txt', datos,fmt='%.2f')
ac= 2.0/32768.0
gy = 250/32768 
for i in range (0,3):
    for j in range (0,201):
       datos[j][i+2]=((datos1[j,i+2])-offset[i])*ac
       datos[j][i+5]=((datos1[j,i+5])-offset[i+3])*gy
for j in range (0,201):
    datos[j][4]=((datos1[j,4])-(offset[2]-(32768/2)))*ac
    
print(datos,"\n")
h = plt.figure (3)
ax3 = h.subplots (2,2)
h.suptitle ('Acelerómetro calibrado MPU6050')
ax3[0,0].plot (datos [:,1], datos[:,2])
ax3[0,0]. set_title ('ax')
ax3[0,1].plot (datos[:,1], datos[:,3])
ax3[0,1]. set_title ('ay')
ax3[1,0].plot (datos[:,1], datos[:,4])
ax3[1,0].set_title ('az')
ax3[1,1].plot (datos[:,1], datos [:, (2,3,4)]) 
ax3[1,1]. set_title ('ax, ay y az')
h.show ()

h1 = plt.figure (4)
ax4 = h1.subplots (2,2)
h1.suptitle ('gyroscopio calibrado MPU6050')
ax4[0,0].plot (datos [:,1], datos[:,5])
ax4[0,0]. set_title ('gx')
ax4[0,1].plot (datos[:,1], datos[:,6])
ax4[0,1]. set_title ('gy')
ax4[1,0].plot (datos[:,1], datos[:,7])
ax4[1,0].set_title ('gz')
ax4[1,1].plot (datos[:,1], datos [:,(5,6,7)]) 
ax4[1,1]. set_title ('gx, gy y gz')
h1.show ()

for i in range (0, 201):
    roll[i][0]=i
    pitch[i][0]=i
    #acelerometro
    roll[i+1][1]=numpy.rad2deg(math.atan2(datos[i,3],datos[i,4]))
    pitch[i+1][1]=numpy.rad2deg(math.atan2(-datos[i,2],math.sqrt(datos[i,3]**2+datos[i,4]**2)))
    #giroscopio
    roll[i+1][2]=roll[i][3]+(numpy.rad2deg(datos[i,5]*dt))
    pitch[i+1][2]=pitch[i][3]+(numpy.rad2deg(datos[i,6]*dt))
    roll[i+1][3]=(A*roll[i+1][2])*(B*roll[i+1][1])
    pitch[i+1][3]=(A*pitch[i+1][2])*(B*pitch[i+1][1])

j=plt.figure(5)
ax5=j.subplots(2,2)
j.suptitle('ANGULO-ROLL')
ax5[0,0].plot (roll [:,0], roll[:,1])
ax5[0,0].set_title ('roll acelerómetro')
ax5[0,0].set_xlabel("Muestras")
ax5[0,0].set_ylabel("Grados")
ax5[0,1].plot (roll [:,0], roll[:,2])
ax5[0,1].set_title ('roll Giroscopio')
ax5[0,1].set_xlabel("Muestras")
ax5[0,1].set_ylabel("Grados")
ax5[1,0].plot (roll [:,0], roll[:,3])
ax5[1,0].set_title ('roll complementario')
ax5[1,0].set_xlabel("Muestras")
ax5[1,0].set_ylabel("Grados")
ax5[1,1].plot (roll [:,0], roll[:,1], label="Acel")
ax5[1,1].plot (roll [:,0], roll[:,2], label="Giro")
ax5[1,1].plot (roll [:,0], roll[:,3], label="FC")
ax5[1,1].set_title ('roll A, G y FC')
ax5[1,1].set_xlabel("Muestras")
ax5[1,1].set_ylabel("Grados")
ax5[1,1].legend(loc='upper left')
j.show()

k=plt.figure(6)
ax6=k.subplots(2,2)
k.suptitle('ANGULO-PITCH')
ax6[0,0].plot (pitch [:,0], pitch[:,1])
ax6[0,0].set_title ('pitch acelerómetro')
ax6[0,0].set_xlabel("Muestras")
ax6[0,0].set_ylabel("Grados")
ax6[0,1].plot (pitch [:,0], pitch[:,2])
ax6[0,1].set_title ('pitch Giroscopio')
ax6[0,1].set_xlabel("Muestras")
ax6[0,1].set_ylabel("Grados")
ax6[1,0].plot (pitch [:,0], pitch[:,3])
ax6[1,0].set_title ('pitch complementario')
ax6[1,0].set_xlabel("Muestras")
ax6[1,0].set_ylabel("Grados")
ax6[1,1].plot (pitch [:,0], pitch[:,1], label="Acel")
ax6[1,1].plot (pitch [:,0], pitch[:,2], label="Giro")
ax6[1,1].plot (pitch [:,0], pitch[:,3], label="FC")
ax6[1,1].set_title ('pitch A, G y FC')
ax6[1,1].set_xlabel("Muestras")
ax6[1,1].set_ylabel("Grados")
ax6[1,1].legend(loc='upper right')
k.show()

for i in range(100):
    conn=requests.get(baseURL+'&field1='+str(datos[1,2]+'&field2=')+str) #NO SE QUE SIGEU, NO VEO UN RABITO :(
    print('ax_c')#.........
    conn.close()
    time.sleep(1)

#hay un else despues, pero no se con cual if va, no muevo nada porque jeisson se molesta :(
#else:
    #print("\nAdios\n")








