fclose(s)
%------------
close all
clear all
clc;
%fclose(instrfind);
delete(instrfind);
%%
s = serial('COM3','BaudRate',9600,'DataBits',8,'Parity','None','StopBits',1);
if strcmp(get(s,'status'),'closed')
    fopen(s)
end
(get(s,'status'))
clc
%
%% serial


%%
%---------------------------
calibra = 1;
k = 1;
fprintf(s,'c')
while(calibra)
    lectura{k} = fscanf(s);
    if(lectura{k}(1) == 'a')
        disp(lectura{k})
       calibra=0
    end
   k  = k +1;
end
clear k
%%
%--------------------
nlectura = length(lectura)-1;
for j=1:nlectura
    temp = cellfun(@str2num,strsplit(lectura{j},';')) ;
    if numel(temp) > 2
        ofsets(j,:) = temp;
    end
end
clear temp
%%
%----------------------------------------------------------------------
i = 1;
 calibra=1
fprintf(s,'h')
while(calibra)
    lectura1{i}= fscanf(s);
    if(lectura1{i}(1) == 'a')
        disp(lectura1{i})
        break
    end
   i  = i +1;
end

%%
%----------
nlectura1 = length(lectura1)-1;
for j=1:nlectura1
    temp1 = cellfun(@str2num,strsplit(lectura1{j},';')) ;
    if numel(temp1) > 2
        valores(j,:) = temp1;
    end
end

%%
%------------------------------
figure;
plot(valores(:,2),valores(:,3),'b')
hold on
plot(valores(:,2),valores(:,4),'r')
plot(valores(:,2),valores(:,5),'g')
title('Acelerometro MPU sin calibrar')
ylabel(' aceleracion (raw)')
xlabel('tiempo (s)')
legend('ax', 'ay', 'az','location','northeast','orientation','vertical')
%%
%--------------
figure;
plot(valores(:,2),valores(:,3)-ofsets(1,1),'b')
hold on
plot(valores(:,2),valores(:,4)-ofsets(1,2),'r')
plot(valores(:,2),valores(:,5)-(ofsets(1,3)-(32768/2)),'g')
title('Acelerometro MPU calibrado')
ylabel(' aceleracion (raw)')
xlabel('tiempo (s)')
legend('ax', 'ay', 'az','location','northeast','orientation','vertical')
%%
%-------
Sc_ac=2.0/32768.0;
Sc_gy=250.0/32768.0;

figure;
plot( valores(:,2),(valores(:,3)-ofsets(1,1) )*Sc_ac,'b')
hold on
plot( valores(:,2),(valores(:,4)-ofsets(1,2))*Sc_ac,'r')
plot( valores(:,2),(valores(:,5)-(ofsets(1,3)-(32768/2)))*Sc_ac,'g')
title('Acelerometro MPU calibrado en escala')
ylabel(' aceleracion (g)')
xlabel('tiempo (s)')
legend('ax', 'ay', 'az','location','northeast','orientation','vertical')
figure;
plot(valores(:,2),(valores(:,3) )*Sc_ac,'b')
hold on
plot(valores(:,2),(valores(:,4) )*Sc_ac,'r')
plot(valores(:,2),(valores(:,5) )*Sc_ac,'g')
title('Acelerometro MPU sin calibrar en escala')
ylabel(' aceleracion (g)')
xlabel('tiempo (s)')
legend('ax', 'ay', 'az','location','northeast','orientation','vertical')
%%
%------------------
figure;
plot(valores(:,2),valores(:,6),'b')
hold on
plot(valores(:,2),valores(:,7),'r')
plot(valores(:,2),valores(:,8),'g')
title('Giroscopio MPU sin calibrar')
ylabel(' velocidad angular (raw)')
xlabel('tiempo (s)')
legend('gx', 'gy', 'gz','location','northeast','orientation','vertical')
%%
figure;
plot(valores(:,2),valores(:,6) - ofsets(1,4),'b')
hold on
plot(valores(:,2),valores(:,7) - ofsets(1,5),'r')
plot(valores(:,2),valores(:,8) - ofsets(1,6),'g')
title('Giroscopio MPU calibrado')
ylabel(' velocidad angula (raw)')
xlabel('tiempo (s)')
legend('gx', 'gy', 'gz','location','northeast','orientation','vertical')
%%
%----------
figure;
plot( valores(:,2),(valores(:,6)-ofsets(1,4) )*Sc_gy,'b')
hold on
plot( valores(:,2),(valores(:,7)-ofsets(1,5))*Sc_gy,'r')
plot( valores(:,2),(valores(:,8)-(ofsets(1,6)))*Sc_gy,'g')
title('Giroscopio MPU calibrado en escala')
ylabel(' Velocidad angular (°/s)')
xlabel('tiempo (s)')
legend('gx', 'gy', 'gz','location','northeast','orientation','vertical')
%%
figure;
plot(valores(:,2),(valores(:,6) )*Sc_ac,'b')
hold on
plot(valores(:,2),(valores(:,7) )*Sc_ac,'r')
plot(valores(:,2),(valores(:,8) )*Sc_ac,'g')
title('Giroscopio MPU sin calibrar en escala')
ylabel(' Velocidad angular (°/s)')
xlabel('tiempo (s)')
legend('gx', 'gy', 'gz','location','northeast','orientation','vertical')

