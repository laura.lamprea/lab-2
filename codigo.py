import serial
import numpy
import matplotlib.pyplot as plt
from copy import copy, deepcopy

data = serial.Serial('/dev/ttyACM0',9600,timeout=10)
print(data)

datos = numpy.zeros((201,8))
datos1 = numpy.zeros((201,8))
datos2 = numpy.zeros((201,8))

value = input("\nQuiere adquirir los offsets s/n \n\n")
if value == 's':
    for j in range(2):
        print("\nCapturando datos \n")
        data.write(b'c')
        offseta = data.readline()
        sobra = data.readline()
        offseta = offseta.decode("utf-8")
        offseta= offseta.split(";")
    print(offseta,"\n")
    print(sobra,"\n")
    offset=[]
    for i in offseta:
        a=float(i)
        offset.append(a)
    print(offset,"\n")
value1 = input("\nQuiere adquirir los datos s/n \n\n")
if value1 == 's':
    data.write(b"h")    
    for i in range(201):
        x=data.readline()
        x=x.decode("utf-8")
        x=x.split(";")
        datos1[i][:]=x
    print(datos1,"\n")
    datos=deepcopy(datos1)
c = numpy.savetxt('geekfile.txt', datos,fmt='%.2f')
ac= 2.0/32768.0
gy = 250/32768 
for i in range (0,3):
    for j in range (0,201):
       datos[j][i+2]=((datos1[j,i+2])-offset[i])*ac
       datos[j][i+5]=((datos1[j,i+5])-offset[i+3])*gy
for j in range (0,201):
    datos[j][4]=((datos1[j,4])-(offset[2]-(32768/2)))*ac
    
print(datos,"\n")
h = plt.figure (3)
ax3 = h.subplots (2,2)
h.suptitle ('Acelerómetro calibrado MPU6050')
ax3[0,0].plot (datos [:,1], datos[:,2])
ax3[0,0]. set_title ('ax')
ax3[0,1].plot (datos[:,1], datos[:,3])
ax3[0,1]. set_title ('ay')
ax3[1,0].plot (datos[:,1], datos[:,4])
ax3[1,0].set_title ('az')
ax3[1,1].plot (datos[:,1], datos [:, (2,3,4)]) 
ax3[1,1]. set_title ('ax, ay y az')
h.show ()

h1 = plt.figure (4)
ax4 = h1.subplots (2,2)
h1.suptitle ('gyroscopio calibrado MPU6050')
ax4[0,0].plot (datos [:,1], datos[:,5])
ax4[0,0]. set_title ('gx')
ax4[0,1].plot (datos[:,1], datos[:,6])
ax4[0,1]. set_title ('gy')
ax4[1,0].plot (datos[:,1], datos[:,7])
ax4[1,0].set_title ('gz')
ax4[1,1].plot (datos[:,1], datos [:,(5,6,7)]) 
ax4[1,1]. set_title ('gx, gy y gz')
h1.show ()



