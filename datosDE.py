from tkinter import messagebox
import serial
import numpy as np
import matplotlib.pyplot as plt
import time 
sac= 1/16384
sgy= 250.0/32768.0
dt=0.1
valor=0
setdat = np.array([[0,0,0,0,0,0,0,0,0,0,0,0]],dtype=float)
setdat1=np.array([[0,0,0,0,0,0,0,0,0]],dtype=float)
ofset = [0 ,0 ,908.00 ,138.00,-2148.0,-404.00,-357.00,55.50,0]
total=0

#data = serial.Serial('/dev/ttyACM0',9600,timeout=10)
if (1):#messagebox.askyesno(message="¿Desea iniciar?", title="Linea o circulo")):

        serialPort1 = serial.Serial(port='COM3', baudrate=9600, timeout=10)
        serialPort1.write(b'h')
        f=0
        maxt=0
        mint=0
        while(1):
            data = serialPort1.readline()
            data = data.decode("utf-8")
            data = data.split(';')
            #print(data)
            data1=[]
            if (len(data)<6):
                break
            data[8]='0'
            for j in data:
                data1.append(float(j))
            #setdat1 = np.append(setdat1,[data1],axis=0)
            for k in range(2,5):
                data1[k]= (data1[k]-ofset[k])*sac
                data1[k+3]= (data1[k+3]-ofset[k+3])*sgy
            arr=np.array(data)
            #print(data1)    
            #angulos de euler
            data1[8]=np.rad2deg(np.arctan2(data1[4],data1[3]))
            data1.append(np.rad2deg( np.arctan2(-data1[2], (np.sqrt(np.power(data1[3],2)+ np.power(data1[4],2)) ))) )
            data1.append(np.rad2deg(data1[6]*dt))
            data1.append(((np.sqrt( np.power(data1[4],2)+ np.power(data1[2],2)+ np.power(data1[3],2)) )*980))
                
            if (valor==0):
                valor=data1[11]
            if (len(setdat)>2):    
                acc=abs(data1[11])-abs(setdat[(len(setdat)-2),11])
                total1=total
                total=abs(acc)+total
                tab=total1-total
                if (abs(tab)>13):
                    if (f==0):
                        mint=data1[1]
                    f=f+1
                    print(np.sqrt( np.power(data1[4],2)+ np.power(data1[2],2)+ np.power(data1[3],2)))
                    if (data1[1]>maxt):
                        maxt=data1[1]
                    
                print("tab",tab)
                
            #data1.append(np.rad2deg(data1[7]*dt))
            setdat = np.append(setdat,[data1],axis=0)
        acdes=0
        if (f>0):
            xd=0
            xd1=0
            xd2=0
            a=int(100*mint)
            b=int(100*maxt)
            maxroll=0
            minroll=0
            maxPITCH=0
            minPITCH=0
            for i in range(a,b):

                if (maxroll<setdat[i,8]):
                    maxroll=setdat[i,8]
                if (minroll>setdat[i,8]):
                    minroll=setdat[i,8]
                if (maxPITCH<setdat[i,9]):
                    maxPITCH=setdat[i,9]
                if (minPITCH>setdat[i,9]):
                    minPITCH=setdat[i,9]

                xd=abs(setdat[i,8])+xd
                xd1=abs(setdat[i,9])+xd1
                xd2=setdat[i,11]+xd2
            dtroll=maxroll+ abs(minroll)
            dtpitch=maxPITCH+abs(minPITCH)
            print(xd/(100*maxt)-(100*mint))
            print(xd1/(100*maxt)-(100*mint))
            acelerpro=(xd2/(100*maxt)-(100*mint))
            print("raw aceleracio",acelerpro)
            print("roll",np.median(setdat[:,8]),"varianza",dtroll)
            print("pitch",np.median(setdat[:,9]),"varianza",dtpitch)
            acdes=acelerpro-np.median(setdat[:,11])
            print("ofset de aceleracion",np.median(setdat[:,11]))
            print("acc",acdes)
        print(f)
        print((maxt-mint))
        constnteacc=f/10*(maxt-mint)
        print(constnteacc)
        #fig,(ax,ax1) = plt.subplots(1,2)
        #fig.suptitle('multi ac')
        #ax.plot(setdat[:,1],setdat[:,8], color="red", label='ax')
        #ax.plot(setdat[:,1],setdat[:,9], color="blue", label='ay')
        #ax1.plot(setdat[:,1],setdat[:,11], color="m", label='gz')
        #ax.get_legend
        #plt.show()
        if ( (abs(acdes)>0) and (dtroll<90) and (constnteacc>1) ):
            acdes=70   
        if ((abs(acdes)<75) and (abs(acdes)>0) and (acelerpro>10) ):
            print ("circulo")
            messagebox.showinfo(message="circulo", title="Resultado")  
        else:
            if ( (abs(acdes)>0) and (acelerpro>10) ):
                print("linea")
                messagebox.showinfo(message="linea", title="resultado")
            else:
                messagebox.showinfo(message="Mueva el esfero", title="resultado")  
else:
    messagebox.showinfo(message="gracias", title="resultado")
    
  


